# Tree Drawers

README / 09 April 2019

-----

## Introduction

Tree Drawers is a lightweight library printing a tree structure reflecting the content of a directory. It is not reserved only for console-based applications as it utilizes [`TextWriter`](https://docs.microsoft.com/dotnet/api/system.io.textwriter).

## Technical information

Name | Value
--- | ---
Main programming language | C#
Framework targeted | .NET Standard 1.3

## Installation

With [NuGet Package Manager](https://docs.microsoft.com/nuget/tools/package-manager-console):

```
Install-Package Fluorite.TreeDrawers
```

With [.NET Core CLI](https://docs.microsoft.com/dotnet/core/tools):

```
dotnet add package Fluorite.TreeDrawers
```

## Usage

1. Create an instance of `DirectoryTreeDrawer` passing [`TextWriter`](https://docs.microsoft.com/dotnet/api/system.io.textwriter) instance which will be used to write output;
2. Call `DrawTree()` passing a path to the desired directory or an instance of [`DirectoryInfo`](https://docs.microsoft.com/dotnet/api/system.io.directoryinfo).

Code below saves the directory tree to the console output:

```c#
var drawer = new Fluorite.TreeDrawers.DirectoryTreeDrawer(System.Console.Out);
var workingDirectoryPath = System.IO.Directory.GetCurrentDirectory();
drawer.DrawTree(workingDirectoryPath);
```

Running the above will produce output similar to this (bit longer in real world):

```
└─ConsoleDemo\
  ├─bin\
  │ └─Debug\
  │   └─netcoreapp2.2\
  │     ├─Fluorite.TreeDrawers.ConsoleDemo.deps.json
  │     ├─Fluorite.TreeDrawers.ConsoleDemo.dll
  │     ├─Fluorite.TreeDrawers.ConsoleDemo.pdb
  │     ├─Fluorite.TreeDrawers.ConsoleDemo.runtimeconfig.dev.json
  │     ├─Fluorite.TreeDrawers.ConsoleDemo.runtimeconfig.json
  │     ├─Fluorite.TreeDrawers.dll
  │     ├─Fluorite.TreeDrawers.pdb
  │     └─Fluorite.TreeDrawers.xml
  ├─ConsoleDemo.csproj
  └─Program.cs
```

##### Save the output to a file:

```c#
var tempFilePath = System.IO.Path.GetTempFileName();
var streamWriter = System.IO.File.CreateText(tempFilePath);
var drawer = new Fluorite.TreeDrawers.DirectoryTreeDrawer(streamWriter);
var workingDirectoryPath = System.IO.Directory.GetCurrentDirectory();
drawer.DrawTree(workingDirectoryPath);
```

See more usage examples [here](https://gitlab.com/fluorite/tree-drawers/tree/master/src/Examples).

## License

Licensed under MIT. Read full license [here](https://gitlab.com/fluorite/tree-drawers/raw/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)
