﻿using System.IO;

namespace Fluorite.TreeDrawers.Examples
{
    public static class DirectoryTreeDrawerExamples
    {
        // Write output to console.
        public static void Main()
        {
            using (var textWriter = System.Console.Out)
            {
                using (var drawer = new Fluorite.TreeDrawers.DirectoryTreeDrawer(textWriter))
                {
                    var workingDirectoryPath = System.IO.Directory.GetCurrentDirectory();
                    drawer.DrawTree(workingDirectoryPath);
                }
            }
        }

        // Write output to a file using StreamWriter.
        public static void StreamWriterExample()
        {
            var tempFilePath = System.IO.Path.GetTempFileName();

            using (var streamWriter = System.IO.File.CreateText(tempFilePath))
            {
                using (var drawer = new Fluorite.TreeDrawers.DirectoryTreeDrawer(streamWriter))
                {
                    var workingDirectoryPath = System.IO.Directory.GetCurrentDirectory();
                    drawer.DrawTree(workingDirectoryPath);
                }
            }
        }

        // Write output to StringWriter, so then it can be passed into variable.
        public static void StringWriterExample()
        {
            var output = string.Empty;

            using (var stringWriter = new StringWriter())
            {
                using (var drawer = new Fluorite.TreeDrawers.DirectoryTreeDrawer(stringWriter))
                {
                    var workingDirectoryPath = System.IO.Directory.GetCurrentDirectory();
                    drawer.DrawTree(workingDirectoryPath);
                }

                output = stringWriter.ToString();
            }

            // Do something with the output.
        }

        // Write output to a StreamContent using StreamWriter created out of MemoryStream.
        // and then send it using HTTP POST request.
        public static async System.Threading.Tasks.Task StreamContentExample()
        {
            using (var memoryStream = new System.IO.MemoryStream())
            {
                using (var streamWriter = new System.IO.StreamWriter(memoryStream))
                {
                    using (var drawer = new Fluorite.TreeDrawers.DirectoryTreeDrawer(streamWriter))
                    {
                        var workingDirectoryPath = System.IO.Directory.GetCurrentDirectory();
                        drawer.DrawTree(workingDirectoryPath);

                        var httpClient = new System.Net.Http.HttpClient();
                        var uri = new System.Uri("http://www.microsoft.com");
                        var streamContent = new System.Net.Http.StreamContent(memoryStream);

                        await httpClient.PostAsync(uri, streamContent);
                    }
                }
            }
        }
    }
}
