﻿using System;
using System.IO;

namespace Fluorite.TreeDrawers
{
    /// <summary>
    /// Provides tree drawing functionalities for directories.
    /// </summary>
    public class DirectoryTreeDrawer : IDisposable
    {
        private readonly TextWriter _textWriter;
        private readonly DirectoryTreeDrawerOptions _options;

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryTreeDrawer"/> class
        /// with specified text writer and default options.
        /// </summary>
        /// <param name="textWriter">
        /// The <see cref="TextWriter"/> which will be used to write output
        /// from the drawer.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="textWriter"/> is <see langword="null"/>.
        /// </exception>
        public DirectoryTreeDrawer(TextWriter textWriter)
        {
            _options = DirectoryTreeDrawerOptions.Default;
            _textWriter = textWriter ?? throw new ArgumentNullException(nameof(textWriter));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryTreeDrawer"/> class
        /// with specified text writer and options.
        /// </summary>
        /// <param name="textWriter">
        /// The <see cref="TextWriter"/> which will be used to write output
        /// from the drawer.
        /// </param>
        /// <param name="options">
        /// The options for drawer.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="textWriter"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="options"/> is <see langword="null"/>.
        /// </exception>
        public DirectoryTreeDrawer(TextWriter textWriter, DirectoryTreeDrawerOptions options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _textWriter = textWriter ?? throw new ArgumentNullException(nameof(textWriter));
        }

        /// <summary>
        /// Draws a directory tree.
        /// </summary>
        /// <param name="directoryPath">
        /// The absolute path to the directory to draw.
        /// </param>
        /// <exception cref="ArgumentException">
        /// <paramref name="directoryPath"/> is <see langword="null"/>, empty
        /// or consists of only whitespace characters.
        /// </exception>
        public void DrawTree(string directoryPath)
        {
            if (string.IsNullOrWhiteSpace(directoryPath))
            {
                throw new ArgumentException(
                    "Provided directory path is null, emtpy " +
                    "or consists of only whitespace characters.",
                    nameof(directoryPath));
            }

            DrawTree(new DirectoryInfo(directoryPath));
        }

        /// <summary>
        /// Draws a directory tree.
        /// </summary>
        /// <param name="directoryInfo">
        /// The absolute path to the directory to draw.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="directoryInfo"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Path in <paramref name="directoryInfo"/> leads to a not existing directory.
        /// </exception>
        public void DrawTree(DirectoryInfo directoryInfo)
        {
            if (directoryInfo == null)
            {
                throw new ArgumentNullException(nameof(directoryInfo));
            }

            if (!Directory.Exists(directoryInfo.FullName))
            {
                throw new ArgumentException(
                    "Cannot draw a tree for a non-existing directory. " +
                    "Please provide a path to an existing directory." +
                    nameof(directoryInfo));
            }

            PrintDirectory(
                directoryInfo,
                prefix: "",
                searchPattern: "*",
                searchOption: SearchOption.TopDirectoryOnly,
                isLast: true,
                isNested: false);
        }

        /// <summary>
        /// Releases all resources used by <see cref="DirectoryTreeDrawer"/>.
        /// </summary>
        public void Dispose()
        {
            _textWriter.Dispose();
        }

        private void PrintDirectory(DirectoryInfo directory, string prefix, string searchPattern, SearchOption searchOption, bool isLast, bool isNested)
        {
            // Print the name of current directory.
            PrintItem(directory.Name, prefix, isLast, directory.Attributes);

            var subDirs = directory.GetDirectories(searchPattern, searchOption);
            var files = directory.GetFiles(searchPattern, searchOption);

            // If this is a "nested" directory, add the parent's connector to the prefix
            prefix += isNested ? "│ " : "  ";

            // Recursively print the content of subdirectories.
            for (var directoryIndex = 0; directoryIndex < subDirs.Length; directoryIndex++)
            {
                var isLastChild = directoryIndex == subDirs.Length - 1 && files.Length == 0;

                // If the parent has files or other directories, mark this as "nested"
                var isNestedDir = files.Length > 0 || !isLastChild;

                PrintDirectory(subDirs[directoryIndex], prefix, searchPattern, searchOption, isLastChild, isNestedDir);
            }

            // Print the names of files located in current directory.
            for (var fileIndex = 0; fileIndex < files.Length; fileIndex++)
            {
                var isLastFile = fileIndex == files.Length - 1;

                PrintItem(files[fileIndex].Name, prefix, isLastFile, files[fileIndex].Attributes);
            }
        }

        private void PrintItem(string name, string prefix, bool isLastItem, FileAttributes attributes)
        {
            // Diferent item connector for last file or directory.
            var itemConnector = isLastItem ? "└─" : "├─";

            // Mark current item with path separator suffix if it is a directory.
            var suffix = attributes.HasFlag(FileAttributes.Directory)
                ? Path.DirectorySeparatorChar.ToString()
                : string.Empty;

            _textWriter?.WriteLine($"{prefix}{itemConnector}{name}{suffix}");
        }
    }
}
