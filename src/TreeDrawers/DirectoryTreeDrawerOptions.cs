﻿using System;

namespace Fluorite.TreeDrawers
{
    /// <summary>
    /// Represents settings for directory tree drawer.
    /// </summary>
    public class DirectoryTreeDrawerOptions
    {
        /// <summary>
        /// Gets the default options for directory tree drawer.
        /// <para/>
        /// This field is read-only.
        /// </summary>
        public static readonly DirectoryTreeDrawerOptions Default = new DirectoryTreeDrawerOptions
        {
            DepthIndent = new DepthIndentationOptions
            {
                Width = 2,
            },
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryTreeDrawerOptions"/>
        /// class.
        /// </summary>
        public DirectoryTreeDrawerOptions()
        {
            DepthIndent = new DepthIndentationOptions();
        }

        /// <summary>
        /// Gets or sets the depth indentation options.
        /// </summary>
        public DepthIndentationOptions DepthIndent { get; set; }

        /// <summary>
        /// Checks if current settings are valid. If not, an appropriate
        /// exception is thrown.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// At least one settings is invalid.
        /// </exception>
        public void Validate()
        {

        }
    }
}
