﻿using System;

namespace Fluorite.TreeDrawers
{
    /// <summary>
    /// Represents settings for depth indentation for directory tree drawer.
    /// </summary>
    public class DepthIndentationOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DepthIndentationOptions"/>
        /// class.
        /// </summary>
        public DepthIndentationOptions()
        {

        }

        /// <summary>
        /// Gets or sets the width of indentation per single level of depth.
        /// <para/>
        /// If set to 0, no depth indentation will be shown.
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Checks if current settings are valid. If not, an appropriate
        /// exception is thrown.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// At least one settings is invalid.
        /// </exception>
        public void Validate()
        {
            if (Width < 0)
            {
                throw new InvalidOperationException($"{Width} cannot have value less zero.");
            }
        }
    }
}
