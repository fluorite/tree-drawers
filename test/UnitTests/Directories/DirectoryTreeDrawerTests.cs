﻿using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Fluorite.TreeDrawers.UnitTests.Directories
{
    public class DirectoryTreeDrawerTests : IDisposable
    {
        private readonly string _tempSubdirectory;

        public DirectoryTreeDrawerTests()
        {
            _tempSubdirectory = CreateRandomDirectory(Path.GetTempPath());
        }

        [Fact]
        public void DirectoryTreeDrawerWorksForEmptyDirectory()
        {
            // Arrange
            var emptyDirectoryPath = CreateRandomDirectory(_tempSubdirectory);
            var tempFilePath = Path.GetTempFileName();
            var streamWriter = File.CreateText(tempFilePath);
            var drawer = new DirectoryTreeDrawer(streamWriter);

            // Act
            drawer.DrawTree(emptyDirectoryPath);
            drawer.Dispose();

            // Assert
            Assert.True(File.Exists(tempFilePath));
            Assert.True(File.ReadAllBytes(tempFilePath).Length > 0);
            Assert.True(File.ReadAllLines(tempFilePath).Length == 1);
        }

        [Fact]
        public void DirectoryTreeDrawerWorksForDirectoryWithThreeFiles()
        {
            // Arrange
            var directoryPath = CreateRandomDirectory(_tempSubdirectory);
            CreateRandomFiles(3, directoryPath);
            var tempFilePath = Path.GetTempFileName();
            var streamWriter = File.CreateText(tempFilePath);
            var drawer = new DirectoryTreeDrawer(streamWriter);

            // Act
            drawer.DrawTree(directoryPath);
            drawer.Dispose();

            // Assert
            Assert.True(File.Exists(tempFilePath));
            Assert.True(File.ReadAllBytes(tempFilePath).Length > 0);
            Assert.True(File.ReadAllLines(tempFilePath).Length == 4);
        }

        public void Dispose()
        {
            Directory.Delete(_tempSubdirectory, recursive: true);
        }

        private string CreateRandomDirectory(string directoryPath)
        {
            var randomDirectoryName = GetRandomDirectoryName();
            var randomDirectoryPath = Path.Combine(directoryPath, randomDirectoryName);
            Directory.CreateDirectory(randomDirectoryPath);

            return randomDirectoryPath;
        }

        private IList<string> CreateRandomDirectories(int amount, string directoryPath)
        {
            var pathList = new List<string>();
            for (int i = 0; i < amount; i++)
            {
                pathList.Add(CreateRandomDirectory(directoryPath));
            }

            return pathList;
        }

        private string CreateRandomFile(string directoryPath)
        {
            var randomFilePath = GetRandomFilePath(directoryPath);
            File.Create(randomFilePath).Close();

            return randomFilePath;
        }

        private IList<string> CreateRandomFiles(int amount, string directoryPath)
        {
            var pathList = new List<string>();
            for (int i = 0; i < amount; i++)
            {
                pathList.Add(CreateRandomFile(directoryPath));
            }

            return pathList;
        }

        private string CreateTempSubdirectory()
        {
            var tempDirectoryPath = Path.GetTempPath();
            var randomDirectoryName = GetRandomDirectoryName();
            var tempSubdirectoryPath = Path.Combine(tempDirectoryPath, randomDirectoryName);
            Directory.CreateDirectory(tempSubdirectoryPath);

            return tempSubdirectoryPath;
        }

        private string GetRandomDirectoryName()
        {
            var randomFileName = Path.GetRandomFileName();
            var randomDirectoryName = Path.GetFileNameWithoutExtension(randomFileName);

            return randomDirectoryName;
        }

        private string GetRandomDirectoryPath(string directoryPath)
        {
            var randomDirectoryName = GetRandomDirectoryName();

            return Path.Combine(directoryPath, randomDirectoryName);
        }

        private string GetRandomFilePath(string directoryPath)
        {
            var randomFileName = Path.GetRandomFileName();

            return Path.Combine(directoryPath, randomFileName);
        }
    }
}
